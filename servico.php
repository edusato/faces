<section id="bgverde-claro">
	<div id="bannermeio-servicos">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<img src="assets/images/thumb05.jpg" width="100%">
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-offset-2 col-md-5 col-lg-offset-2 col-lg-5">
					<div id="descricao-servico">
						<h1>Como podemos te ajudar com os tratamentos de preenchimentos</h1>
						<p>O preenchimento facial é um dos procedimento mais utilizados para a estética. Feito com ácido hialurônico ou toxina botulínica, ajuda a suavizar rugas e marcas de expressão em regiões como a testa, ao redor dos olhos (os "pés de galinha") e no bigode chinês, e é usado também para dar volume.  É uma técnica imprescindível para o rejuvenescimento. De forma muito rápida, um sulco, uma depressão no rosto ou um lábio pode ser melhorado através dessa técnica.</p>
						
						<p>Um dos melhores e o mais seguro produto para o preenchimento facial é, sem dúvida, o ácido hialurônico, que é uma substância produzida naturalmente pelo nosso organismo e está presente principalmente na pele. Tem como função reter água, conferindo hidratação e volume. Gradualmente, com o passar do tempo e o processo natural de envelhecimento, o ácido hialurônico se degrada e o organismo diminui sua capacidade de produção. O resultado é a perda de volume, de contorno e o aparecimento de rugas.</p>

						<p>As principais marcas de ácido hialurônico liberadas no Brasil são produzidas em laboratórios, de uma forma sintética, mas são semelhantes ao componente natural da pele, assim não causam alergias ou rejeições.</p>

						<p>A durabilidade do preenchimento varia de pessoa para pessoa e também conforme a técnica e o local aplicado, podendo durar de 09 a 18 meses, quando utilizado o Ácido Hialurônico.</p>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
					<div id="endereco-clinica">
						<p><strong>AGENDE</strong> SEU HORÁRIO</p>
						<p>Fale com a gente agora, marque um horário e conheça o nosso espaço</p>
						<h3>45 3038 3080</h3>
						<p>
							<a href="#"><i class="fab fa-instagram"></i></a>
							<a href="#"><i class="fab fa-facebook-square"></i></a>
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>