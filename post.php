<section id="bgverde-claro">
	<section id="banner">
		<ul class="bxslider">
			<li><img src="assets/images/banner.jpg" width="100%"></li>
		</ul>
	</section>
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 bgvermelho">
				<img src="assets/images/thumb06.jpg" width="100%">
			</div>
			<div class="col-xs-12 col-sm-12 col-md-offset-1 col-md-10 col-lg-offset-1 col-lg-10">
				<div id="sobre-blog" class="post-interno">
					<p><span>03 JULHO 2018</span></p>
					<h1>Peeling, um dos tratamentos mais utilizados na estética</h1>
					<p>Peeling é um recurso muito utilizado tanto por médicos como outros profissionais da estética. O peeling químico, também chamado de resurfacing químico, quimioesfoliação ou quimiocirurgia, consiste na aplicação de um ou mais agentes cáusticos à pele, produzindo uma destruição controlada da epiderme e sua reepitelialização. Os peelings podem ser classificados segundo o agente indutor da descamação como mecânicos, que variam desde receitas caseiras como cristais de açúcar com fubá, lixas, cremes abrasivos com microesferas de material plástico aos aparelhos de microdermoabrasão por fluxo de cristais ou as lixas de ponta de diamante; físicos, laser, gelo seco e químicos através do uso de substância(s) química(s) isoladas ou combinadas no intuito de se obter o agente mais adequado a cada caso, para graus variados de esfoliação.</p>

					<p>A profundidade do peeling varia entre, muito superficial, superficial, médio e profundo e as complicações dos peelings aumentam de acordo com a profundidade, portanto quanto mais profundo, maior o risco de complicações. Um peeling superficial é incapaz de causar hipo ou hiperpigmentação ou ainda cicatrizes, já nos peelings profundos estas complicações podem ser observadas.</p>

					<p>Apesar das possíveis complicações, o tratamento com peeling ainda é um dos recursos mais utilizados no ramo da estética. O campo de atuação do profissional esteticista limita-se a epiderme e de acordo com a ANVISA a utilização dos alfa-hidroxiácidos (AHA’s) em cosméticos é permitido em pH 3,5 e concentração de 10%.  Mas em um aspecto geral, os problemas relacionados à pele que levam uma pessoa a buscar o auxílio de um profissional esteticista ocorre geralmente na epiderme, desta forma, não há necessidade de atuar em níveis mais profundos.</p>

					<p>Dos benefícios de uso dos AHA’s observa-se a redução da adesão dos corneócitos, espessamento epidérmico, compactação do estrato córneo, aumento na deposição de mucina estimulando também a síntese de fibras colágenas dérmicas. Os AHA’s atuam sobre os corneócitos germinativos, primitivos, profundos de dentro para fora. As indicações são fotoenvelhecimento, acne, eczema hiperquerostático, queratose.</p>
					<p><img src="assets/images/img01.jpg" width="100%"></p>
					<p>Os resultados do peeling químico dependem do tipo de peeling e das características da pele, sendo que peles claras apresentam efeitos mais satisfatórios.</p>
					<p>É necessário que o profissional faça uma avaliação detalhada da pele antes do tratamento com o peeling, principalmente em peles envelhecidas, com maiores chances de lesões. O profissional deve avaliar se não há nenhuma lesão pré-cancerígena ou já cancerígena, pois nesse caso não é indicado o peeling químico.</p>
				</div>
			</div>
		</div>

	</div>

	
</section>