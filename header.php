<!DOCTYPE html>
<html lang="pt-br" class="js flexbox canvas canvastext webgl no-touch geolocation postmessage websqldatabase no-indexeddb hashchange history draganddrop websockets rgba hsla multiplebgs backgroundsize borderimage borderradius boxshadow textshadow opacity cssanimations csscolumns cssgradients cssreflections csstransforms csstransforms3d csstransitions fontface generatedcontent video audio localstorage sessionstorage webworkers applicationcache svg inlinesvg smil svgclippaths">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width,user-scalable=no,initial-scale=1, minimum-scale=1, maximum-scale=1">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="mobile-web-app-capable" content="yes">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no"/> 
<meta name="keywords" content=""/>

<title>DEV MOBI</title>
<!-- css -->
<link rel="stylesheet" href="assets/scripts/components/bootstrap/dist/css/bootstrap.min.css">
<link rel="stylesheet" href="assets/scripts/components/bootstrap/dist/css/bootstrap-theme.min.css">
<link rel="stylesheet" href="assets/styles/style.min.css" />
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
<link rel="stylesheet" href="assets/scripts/components/bxslider-4/src/css/jquery.bxslider.css" />
<link rel="stylesheet" href="assets/styles/css/animate.min.css">
<link rel="stylesheet" href="assets/scripts/components/sweetalert2/dist/sweetalert2.min.css" type="text/css">
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />


</head>
<body>
	<section id="menumobile">
		<div class="menu-switch"><i class="fa fa-bars"></i></div>
		<div class="menu-overlay"></div>
		<nav id="menuresponsivo">
			<div class="menu-switch"><i class="fa fa-bars"></i></div>
			<ul>
				<li class="title">MENU</li>
				<li><a href="index.php">Home</a></li>
				<li><a href="?pag=quem-somos">Quem Somos</a></li>
				<li><a href="?pag=servicos">Serviços</a></li>
				<li><a href="?pag=blog">Blog</a></li>
				<li><a href="?pag=contato">Contato</a></li>
			</ul>
		</nav>
	</section>
	<section id="site">
	<header>
		<div id="header-top" class="hidden-xs hidden-sm">
			<div class="container">
				<div class="row">
					<div class="col-offset-7 col-md-4 col-lg-offset-7 col-lg-4">
						<div id="horario-top">
							AGENDE SEU HORÁRIO <strong>45 3038 3080</strong>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-offset-1 col-md-4 col-lg-offset-1 col-lg-4">
					<div id="logo">
						<a href="index.php">
							<img src="assets/images/logo.png" alt="" class="img-responsive">
						</a>
					</div>
				</div>
				<div class="hidden-xs hidden-sm col-md-offset-1 col-md-5 col-lg-offset-1 col-lg-5">
					<nav id="menu">
						<ul>
							<li><a href="?pag=quem-somos">Quem Somos</a></li>
							<li><a href="?pag=servicos">Serviços</a></li>
							<li><a href="?pag=blog">Blog</a></li>
							<li><a href="?pag=contato">Contato</a></li>
						</ul>
					</nav>
				</div>
			</div>
		</div>
	</header><!-- /header -->