<section id="banner">
	<ul class="bxslider">
		<li><img src="assets/images/banner.jpg" width="100%"></li>
	</ul>
</section>

<section id="destaques">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 titulopag">
				<h1>Destaques</h1>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
				<div class="box-destaque">
					<img src="timthumb.php?src=assets/images/thumb01.jpg&w=350&h=469&q=100" alt="">
					<div class="descricao-box-destaque">
						<h2>Toxina Botulínica - Botox®</h2>
						<p>O Botox é uma marca da substância Toxina botulínica, derivada de bactérias, descoberta a mais de um século e utilizada desde os anos 80 para fins medicinais. O Dysport e o Prosigne são outras marcas muito utilizadas no Brasil.</p>
						<p><a class="lermais" href="?pag=servico">Ler mais <i class="fas fa-angle-right"></i></a></p>
					</div>
				</div>
			</div>

			<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
				<div class="box-destaque">
					<img src="timthumb.php?src=assets/images/thumb02.jpg&w=350&h=469&q=100" alt="">
					<div class="descricao-box-destaque">
						<h2>Toxina Botulínica - Botox®</h2>
						<p>O Botox é uma marca da substância Toxina botulínica, derivada de bactérias, descoberta a mais de um século e utilizada desde os anos 80 para fins medicinais. O Dysport e o Prosigne são outras marcas muito utilizadas no Brasil.</p>
						<p><a class="lermais" href="?pag=servico">Ler mais <i class="fas fa-angle-right"></i></a></p>
					</div>
				</div>
			</div>

			<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
				<div class="box-destaque">
					<img src="timthumb.php?src=assets/images/thumb03.jpg&w=350&h=469&q=100" alt="">
					<div class="descricao-box-destaque">
						<h2>Toxina Botulínica - Botox®</h2>
						<p>O Botox é uma marca da substância Toxina botulínica, derivada de bactérias, descoberta a mais de um século e utilizada desde os anos 80 para fins medicinais. O Dysport e o Prosigne são outras marcas muito utilizadas no Brasil.</p>
						<p><a class="lermais" href="?pag=servico">Ler mais <i class="fas fa-angle-right"></i></a></p>
					</div>
				</div>
			</div>

		</div>
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 btn-vermais">
				<hr>
				<a href="#">Conheça todos os nossos serviços</a>
			</div>
		</div>
	</div>
</section>

<section id="ultimos-artigos">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 titulopag">
				<h1>Últimos artigos</h1>
			</div>
		</div>
		<div class="row">
			
			<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
				<div class="box-artigo">
					<img src="timthumb.php?src=assets/images/img01.jpg&w=350&h=300&q=100" alt="">
					<div class="descricao-box-artigo">
						<h2>Toxina Botulínica - Botox®</h2>
						<p>O Botox é uma marca da substância Toxina botulínica, derivada de bactérias, descoberta a mais de um século e utilizada desde os anos 80 para fins medicinais. O Dysport e o Prosigne são outras marcas muito utilizadas no Brasil.</p>
						<p><a class="lermais" href="?pag=post">Ler mais <i class="fas fa-angle-right"></i></a></p>
					</div>
				</div>
			</div>

			<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
				<div class="box-artigo">
					<img src="timthumb.php?src=assets/images/img02.jpg&w=350&h=300&q=100" alt="">
					<div class="descricao-box-artigo">
						<h2>Toxina Botulínica - Botox®</h2>
						<p>O Botox é uma marca da substância Toxina botulínica, derivada de bactérias, descoberta a mais de um século e utilizada desde os anos 80 para fins medicinais. O Dysport e o Prosigne são outras marcas muito utilizadas no Brasil.</p>
						<p><a class="lermais" href="?pag=post">Ler mais <i class="fas fa-angle-right"></i></a></p>
					</div>
				</div>
			</div>

			<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
				<div class="box-artigo">
					<img src="timthumb.php?src=assets/images/img03.jpg&w=350&h=300&q=100" alt="">
					<div class="descricao-box-artigo">
						<h2>Toxina Botulínica - Botox®</h2>
						<p>O Botox é uma marca da substância Toxina botulínica, derivada de bactérias, descoberta a mais de um século e utilizada desde os anos 80 para fins medicinais. O Dysport e o Prosigne são outras marcas muito utilizadas no Brasil.</p>
						<p><a class="lermais" href="?pag=post">Ler mais <i class="fas fa-angle-right"></i></a></p>
					</div>
				</div>
			</div>

		</div>
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 btn-vermais">
				<hr>
				<a href="#">Ler mais artigos no blog</a>
			</div>
		</div>
	</div>
</section>

<section id="banner-mini">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-offset-1 col-md-10 col-lg-offset-1 col-lg-10">
				<ul class="bxslider-mini">
					<li><img src="assets/images/mini-banner.jpg" alt="" width="100%"></li>
					<li><img src="assets/images/mini-banner.jpg" alt="" width="100%"></li>
				</ul>
			</div>
		</div>
	</div>
</section>