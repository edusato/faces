<section id="bgverde-claro">
	<section id="banner">
		<div id="sobrebanner">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-offset-2 col-md-4 col-lg-offset-2 col-lg-4 infobanner">
						<h2>Tratamentos estéticos e procedimentos da Faces</h2>
						<p>Tratamentos estéticos não existem somente para garantir beleza. Eles podem proporcionar bem-estar e qualidade de vida para quem utiliza.</p>
					</div>
				</div>
			</div>
		</div>
		<ul class="bxslider">
			<li><img src="assets/images/banner.jpg" width="100%"></li>
		</ul>
	</section>

	
	<section id="servicos">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
					<a href="?pag=servico">
						<div class="box-servico">
							<img src="timthumb.php?src=assets/images/img01.jpg&w=240&h=230&q=100" alt="">
							<h2>Dermatologia Clínica</h2>
							<p>A Dermatologia Clínica é a área da dermatologia que tem como foco as doenças que afetam a pele, os cabelos e as unhas. A Dermatologia Clínica tem como especialidade os tratamentos fisiopatológicos, desde tumores a infecções e inflamações, como a acne, por exemplo.</p>
						</div>
					</a>
				</div>
				
				<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
					<a href="?pag=servico">
						<div class="box-servico">
							<img src="timthumb.php?src=assets/images/img02.jpg&w=240&h=230&q=100" alt="">
							<h2>Dermatologia Clínica</h2>
							<p>A Dermatologia Clínica é a área da dermatologia que tem como foco as doenças que afetam a pele, os cabelos e as unhas. A Dermatologia Clínica tem como especialidade os tratamentos fisiopatológicos, desde tumores a infecções e inflamações, como a acne, por exemplo.</p>
						</div>
					</a>
				</div>

				<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
					<a href="?pag=servico">
						<div class="box-servico">
							<img src="timthumb.php?src=assets/images/img03.jpg&w=240&h=230&q=100" alt="">
							<h2>Dermatologia Clínica</h2>
							<p>A Dermatologia Clínica é a área da dermatologia que tem como foco as doenças que afetam a pele, os cabelos e as unhas. A Dermatologia Clínica tem como especialidade os tratamentos fisiopatológicos, desde tumores a infecções e inflamações, como a acne, por exemplo.</p>
						</div>
					</a>
				</div>

				<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
					<a href="?pag=servico">
						<div class="box-servico">
							<img src="timthumb.php?src=assets/images/img04.jpg&w=240&h=230&q=100" alt="">
							<h2>Dermatologia Clínica</h2>
							<p>A Dermatologia Clínica é a área da dermatologia que tem como foco as doenças que afetam a pele, os cabelos e as unhas. A Dermatologia Clínica tem como especialidade os tratamentos fisiopatológicos, desde tumores a infecções e inflamações, como a acne, por exemplo.</p>
						</div>
					</a>
				</div>

				<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
					<a href="?pag=servico">
						<div class="box-servico">
							<img src="timthumb.php?src=assets/images/img05.jpg&w=240&h=230&q=100" alt="">
							<h2>Dermatologia Clínica</h2>
							<p>A Dermatologia Clínica é a área da dermatologia que tem como foco as doenças que afetam a pele, os cabelos e as unhas. A Dermatologia Clínica tem como especialidade os tratamentos fisiopatológicos, desde tumores a infecções e inflamações, como a acne, por exemplo.</p>
						</div>
					</a>
				</div>

				<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
					<a href="?pag=servico">
						<div class="box-servico">
							<img src="timthumb.php?src=assets/images/img06.jpg&w=240&h=230&q=100" alt="">
							<h2>Dermatologia Clínica</h2>
							<p>A Dermatologia Clínica é a área da dermatologia que tem como foco as doenças que afetam a pele, os cabelos e as unhas. A Dermatologia Clínica tem como especialidade os tratamentos fisiopatológicos, desde tumores a infecções e inflamações, como a acne, por exemplo.</p>
						</div>
					</a>
				</div>

				<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
					<a href="?pag=servico">
						<div class="box-servico">
							<img src="timthumb.php?src=assets/images/img07.jpg&w=240&h=230&q=100" alt="">
							<h2>Dermatologia Clínica</h2>
							<p>A Dermatologia Clínica é a área da dermatologia que tem como foco as doenças que afetam a pele, os cabelos e as unhas. A Dermatologia Clínica tem como especialidade os tratamentos fisiopatológicos, desde tumores a infecções e inflamações, como a acne, por exemplo.</p>
						</div>
					</a>
				</div>

				<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
					<a href="?pag=servico">
						<div class="box-servico">
							<img src="timthumb.php?src=assets/images/img08.jpg&w=240&h=230&q=100" alt="">
							<h2>Dermatologia Clínica</h2>
							<p>A Dermatologia Clínica é a área da dermatologia que tem como foco as doenças que afetam a pele, os cabelos e as unhas. A Dermatologia Clínica tem como especialidade os tratamentos fisiopatológicos, desde tumores a infecções e inflamações, como a acne, por exemplo.</p>
						</div>
					</a>
				</div>

			</div>
		</div>
	</section>

</section>