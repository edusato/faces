<section id="bgverde-claro">
	<section id="banner">
		<ul class="bxslider">
			<li><img src="assets/images/banner.jpg" width="100%"></li>
		</ul>
	</section>
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 bgvermelho">
				<img src="assets/images/thumb06.jpg" width="100%">
			</div>
			<div class="col-xs-12 col-sm-12 col-md-offset-1 col-md-10 col-lg-offset-1 col-lg-10">
				<div id="sobre-blog">
					<h2>Peeling, um dos tratamentos mais utilizados na estética</h2>
					<p><span>03 JULHO 2018</span></p>
					<p>Peeling é um recurso muito utilizado tanto por médicos como outros profissionais da estética. O peeling químico, também chamado de resurfacing químico, quimioesfoliação ou quimiocirurgia, consiste na aplicação de um ou mais agentes cáusticos à pele, produzindo uma destruição controlada da epiderme e sua reepitelialização. Os peelings podem ser classificados segundo o agente indutor da descamação como mecânicos, que variam desde receitas caseiras como cristais de açúcar com fubá, lixas, cremes abrasivos com microesferas de material plástico aos aparelhos  [...]</p>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
				<a href="?pag=post">
					<div class="box-blog">
						<img src="timthumb.php?src=assets/images/img01.jpg&w=355&h=230&q=100" alt="">
						<div class="desc-post-blog">
							<span class="datapost">03 JULHO 2018</span>
							<h2>Esfoliação na Limpeza de Pele Tipos e Finalidades</h2>
							<p>A esfoliação da pele é uma etapa fundamental da limpeza de pele, pois irá afinar a camada superficial da epiderme favorecendo a remoção da sujidade mais profunda, melhor emoliência, além de promover a renovação celular e a epitelização, o que favorece a permeação dos princípios ativos. Há vários tipos de esfoliantes. Para uso no [...]</p>
						</div>
					</div>
				</a>
			</div>

			<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
				<a href="?pag=post">
					<div class="box-blog">
						<img src="timthumb.php?src=assets/images/img02.jpg&w=355&h=230&q=100" alt="">
						<div class="desc-post-blog">
							<span class="datapost">03 JULHO 2018</span>
							<h2>Esfoliação na Limpeza de Pele Tipos e Finalidades</h2>
							<p>A esfoliação da pele é uma etapa fundamental da limpeza de pele, pois irá afinar a camada superficial da epiderme favorecendo a remoção da sujidade mais profunda, melhor emoliência, além de promover a renovação celular e a epitelização, o que favorece a permeação dos princípios ativos. Há vários tipos de esfoliantes. Para uso no [...]</p>
						</div>
					</div>
				</a>
			</div>

			<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
				<a href="?pag=post">
					<div class="box-blog">
						<img src="timthumb.php?src=assets/images/img03.jpg&w=355&h=230&q=100" alt="">
						<div class="desc-post-blog">
							<span class="datapost">03 JULHO 2018</span>
							<h2>Esfoliação na Limpeza de Pele Tipos e Finalidades</h2>
							<p>A esfoliação da pele é uma etapa fundamental da limpeza de pele, pois irá afinar a camada superficial da epiderme favorecendo a remoção da sujidade mais profunda, melhor emoliência, além de promover a renovação celular e a epitelização, o que favorece a permeação dos princípios ativos. Há vários tipos de esfoliantes. Para uso no [...]</p>
						</div>
					</div>
				</a>
			</div>

			<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
				<a href="?pag=post">
					<div class="box-blog">
						<img src="timthumb.php?src=assets/images/img04.jpg&w=355&h=230&q=100" alt="">
						<div class="desc-post-blog">
							<span class="datapost">03 JULHO 2018</span>
							<h2>Esfoliação na Limpeza de Pele Tipos e Finalidades</h2>
							<p>A esfoliação da pele é uma etapa fundamental da limpeza de pele, pois irá afinar a camada superficial da epiderme favorecendo a remoção da sujidade mais profunda, melhor emoliência, além de promover a renovação celular e a epitelização, o que favorece a permeação dos princípios ativos. Há vários tipos de esfoliantes. Para uso no [...]</p>
						</div>
					</div>
				</a>
			</div>

			<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
				<a href="?pag=post">
					<div class="box-blog">
						<img src="timthumb.php?src=assets/images/img05.jpg&w=355&h=230&q=100" alt="">
						<div class="desc-post-blog">
							<span class="datapost">03 JULHO 2018</span>
							<h2>Esfoliação na Limpeza de Pele Tipos e Finalidades</h2>
							<p>A esfoliação da pele é uma etapa fundamental da limpeza de pele, pois irá afinar a camada superficial da epiderme favorecendo a remoção da sujidade mais profunda, melhor emoliência, além de promover a renovação celular e a epitelização, o que favorece a permeação dos princípios ativos. Há vários tipos de esfoliantes. Para uso no [...]</p>
						</div>
					</div>
				</a>
			</div>

			<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
				<a href="?pag=post">
					<div class="box-blog">
						<img src="timthumb.php?src=assets/images/img06.jpg&w=355&h=230&q=100" alt="">
						<div class="desc-post-blog">
							<span class="datapost">03 JULHO 2018</span>
							<h2>Esfoliação na Limpeza de Pele Tipos e Finalidades</h2>
							<p>A esfoliação da pele é uma etapa fundamental da limpeza de pele, pois irá afinar a camada superficial da epiderme favorecendo a remoção da sujidade mais profunda, melhor emoliência, além de promover a renovação celular e a epitelização, o que favorece a permeação dos princípios ativos. Há vários tipos de esfoliantes. Para uso no [...]</p>
						</div>
					</div>
				</a>
			</div>

		</div>
	</div>

	
</section>