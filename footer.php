<footer>
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<img src="assets/images/logo.png" height="50px" alt="">
				<p><strong>45 3038 3080</strong></p>
				<p>Edifício day saúde<br/>RUA minas gerais, 2061, 8º andar • cascavel - pr</p>
				<p>
					<a href="#"><i class="fab fa-instagram"></i></a>
					<a href="#"><i class="fab fa-facebook-square"></i></a>
				</p>
			</div>
		</div>
	</div>
</footer>
</section><!-- SECTION SITE -->
<!-- scripts -->
<script src="assets/scripts/components/modernizr/modernizr.js" type="text/javascript"></script>
<script src="assets/scripts/components/jquery/dist/jquery.js" type="text/javascript"></script>
<script src="assets/scripts/components/colorbox/jquery.colorbox-min.js" type="text/javascript"></script>
<script src="assets/scripts/components/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
<script src="assets/scripts/components/bxslider-4/src/js/jquery.bxslider.js" type="text/javascript"></script>
<script src="assets/scripts/components/bxslider-4/src/vendor/jquery.easing.1.3.js" type="text/javascript"></script>
<script src="assets/scripts/components/sweetalert2/dist/sweetalert2.min.js"></script>
<script src="assets/scripts/components/app.js" type="text/javascript"></script>
<?php //include('controller.php'); ?>
</body>
</html>