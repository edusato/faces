<section id="banner">
	<ul class="bxslider">
		<li><img src="assets/images/banner.jpg" width="100%"></li>
	</ul>
</section>
<div class="container">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-offset-1 col-md-10 col-lg-offset-1 col-lg-10 bgrosa">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-offset-1 col-md-5 col-lg-offset-1 col-lg-5">
					<div id="formulario">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 pd0">
							<input type="text" name="nome" placeholder="NOME">
						</div>
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 pd0">
							<input type="text" name="email" placeholder="E-MAIL">
						</div>
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 pd0">
							<input type="text" name="assunto" placeholder="ASSUNTO">
						</div>
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 pd0">
							<textarea name="msg" placeholder="MENSAGEM"></textarea>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 pd0">
							<input type="submit" value="ENVIAR">
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
					<div id="infocontato">
						<p>ATENDIMENTO</p>
						<p><strong>Segunda a Sexta das 8h às 12h - 13h30 às 18h</strong></p>
						<h3>45 3038 3080</h3>
						<p>CONTATO@CENTROMEDICOFACES.COM.BR</p>
						<p>EDIFÍCIO DAY SAÚDE<br/>
						R. MINAS GERAIS, 2061 • 8º ANDAR<br/>
						cascavel - pr</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-offset-1 col-md-10 col-lg-offset-1 col-lg-10 pd0">
			<div id="mapa">
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3617.49421428678!2d-53.45250718499541!3d-24.949294684012013!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94f3d47225e7e541%3A0x1fbf5189aedee238!2sR.+Minas+Gerais%2C+2061+-+Centro%2C+Cascavel+-+PR%2C+85812-030!5e0!3m2!1spt-BR!2sbr!4v1536092886239" width="100%" height="435" frameborder="0" style="border:0" allowfullscreen></iframe>
			</div>
		</div>
	</div>
</div>