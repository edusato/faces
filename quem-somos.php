<section id="bgsobre">
	<div class="container">
		<div class="row" style="position: relative;">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<img src="assets/images/thumb04.jpg" width="100%">
			</div>
			<div id="descricao-sobre">
				<p>Na Faces Centro Médico Integrado, equilíbrio e serenidade estão igualmente integrados.</p>
				<p>A saúde plena e o bem-estar são, acima de tudo, os principais pilares na busca pelo corpo em harmonia: com a saúde, com a mente e com o mundo.</p>
			</div>
		</div>
	</div>
</section>
<section id="banner-mini-sobre">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-offset-1 col-md-10 col-lg-offset-1 col-lg-10">
				<ul class="bxslider-mini">
					<li><img src="assets/images/mini-banner.jpg" alt="" width="100%"></li>
					<li><img src="assets/images/mini-banner.jpg" alt="" width="100%"></li>
				</ul>
			</div>
		</div>
	</div>
</section>

<section id="conteudo-sobre">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-offset-2 col-md-8 col-lg-offset-2 col-lg-8">
				<h2>O corpo clínico Faces é composto por profissionais de diferentes áreas, mas todos com a mesma essência: cordialidade, respeito e profissionalismo. Um centro integrado cuja medicina trabalha em comunhão com a individualidade de cada paciente.</h2>
			</div>
		</div>
	</div>
</section>

<section id="doutores">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
				<div class="box-drs">
					<img src="timthumb.php?src=assets/images/img01.jpg&w=180&h=180&q=100" alt="">
					<h2>Dra. Karine Dall'Oglio Tolazzi</h2>
					<p><span>CRM-PR 21.910</span></p>
					<p>Membro da Sociedade Brasileira de Dermatologia (SBD)</p>
					<p>Formada em Medicina pela Pontífica Universidade Católica do Paraná</p>
					<p>Especialização em Dermatologia pela Santa Casa de Misericórdia de Curitiba (PUC-PR)</p>
					<p>Membro da Sociedade Brasileira de Dermatologia Fellow em Dermatocosmiatria pela Universidade de Miami - Departamento de Dermatologia</p>
					<p>Pós Graduação em Dermatocosmiatria pelo Serviço de Dermatologia do ABC Paulista</p>
				</div>
			</div>

			<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
				<div class="box-drs">
					<img src="timthumb.php?src=assets/images/img02.jpg&w=180&h=180&q=100" alt="">
					<h2>Dra. Karine Dall'Oglio Tolazzi</h2>
					<p><span>CRM-PR 21.910</span></p>
					<p>Membro da Sociedade Brasileira de Dermatologia (SBD)</p>
					<p>Formada em Medicina pela Pontífica Universidade Católica do Paraná</p>
					<p>Especialização em Dermatologia pela Santa Casa de Misericórdia de Curitiba (PUC-PR)</p>
					<p>Membro da Sociedade Brasileira de Dermatologia Fellow em Dermatocosmiatria pela Universidade de Miami - Departamento de Dermatologia</p>
					<p>Pós Graduação em Dermatocosmiatria pelo Serviço de Dermatologia do ABC Paulista</p>
				</div>
			</div>

			<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
				<div class="box-drs">
					<img src="timthumb.php?src=assets/images/img03.jpg&w=180&h=180&q=100" alt="">
					<h2>Dra. Karine Dall'Oglio Tolazzi</h2>
					<p><span>CRM-PR 21.910</span></p>
					<p>Membro da Sociedade Brasileira de Dermatologia (SBD)</p>
					<p>Formada em Medicina pela Pontífica Universidade Católica do Paraná</p>
					<p>Especialização em Dermatologia pela Santa Casa de Misericórdia de Curitiba (PUC-PR)</p>
					<p>Membro da Sociedade Brasileira de Dermatologia Fellow em Dermatocosmiatria pela Universidade de Miami - Departamento de Dermatologia</p>
					<p>Pós Graduação em Dermatocosmiatria pelo Serviço de Dermatologia do ABC Paulista</p>
				</div>
			</div>
		</div>
	</div>
</section>